package com.chon.microservice.authen;

import com.chon.microservice.authen.dto.CommonResponseDto;
import com.chon.microservice.authen.dto.LoginDto;
import com.chon.microservice.authen.dto.TokenDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoginIntegrationTest {
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void createClient() {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("admin");
        loginDto.setPassword("1234");

        ResponseEntity<CommonResponseDto> responseEntity =
                restTemplate.postForEntity("/authentication/login", loginDto, CommonResponseDto.class);
        CommonResponseDto loginResponse = responseEntity.getBody();


        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(loginResponse.getStatus()).isEqualTo("200");
    }

    @Test
    public void verify() {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("admin");
        loginDto.setPassword("1234");

        ResponseEntity<CommonResponseDto> responseEntity =
                restTemplate.postForEntity("/authentication/login", loginDto, CommonResponseDto.class);
        CommonResponseDto loginResponse = responseEntity.getBody();


        TokenDto tokenDto = mapper.convertValue(loginResponse.getData(),TokenDto.class);
        ResponseEntity<CommonResponseDto> verifyEntity =
                restTemplate.postForEntity("/authentication/verify", tokenDto, CommonResponseDto.class);
        CommonResponseDto verifyResponse = verifyEntity.getBody();


        assertThat(verifyEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(verifyResponse.getStatus()).isEqualTo("200");
    }

}
