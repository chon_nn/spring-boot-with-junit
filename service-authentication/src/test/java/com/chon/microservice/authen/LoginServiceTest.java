package com.chon.microservice.authen;

import com.chon.microservice.authen.dto.CommonResponseDto;
import com.chon.microservice.authen.dto.LoginDto;
import com.chon.microservice.authen.dto.TokenDto;
import com.chon.microservice.authen.repository.UserRepository;
import com.chon.microservice.authen.service.LoginService;
import com.chon.microservice.authen.service.TokenProvider;
import com.chon.microservice.authen.util.HashUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private TokenProvider tokenProvider;

    @Mock
    private HashUtil hashUtil;

    private LoginService loginService;

    @Before
    public void setUp() {
        loginService = new LoginService(hashUtil,userRepository,tokenProvider);
    }

    @Test
    public void testLogin() {

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername("admin");
        loginDto.setUsername("1234");
        CommonResponseDto result = loginService.login(loginDto);

        assertEquals("200", result.getStatus());
    }

    @Test
    public void testVerify(){

        TokenDto tokenDto = new TokenDto();
        tokenDto.setAccessToken("eyJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50X2RhdGUiOiIyMDIwLTA3LTEzVDIyOjE2OjQ2IiwidXNlcm5hbWUiOiJhZG1pbiIsInN0YXR1cyI6IkEifQ.VJkPz6Rrwiz6TMcDy1FYj4iLtVo4o6XW-j73NgGFyAE");
        CommonResponseDto result = loginService.verify(tokenDto);

        assertEquals("200", result.getStatus());
    }

}
