package com.chon.microservice.authen.repository;

import com.chon.microservice.authen.dto.UserDto;
import com.chon.microservice.authen.entity.UserTokenEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class UserRepository {
    final private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserDto findUserByUsernameAndPassword(String username,String password){

        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select username,status from users where username=:username and password=:password ");

        params.put("username",username);
        params.put("password",password);

        return namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(UserDto.class));
    }

    public UserDto findUserByToken(String token){

        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select username,status from user_token where token=:token ");

        params.put("token",token);

        return namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(UserDto.class));
    }

    public Integer saveUserToken(UserTokenEntity userTokenEntity){
        StringBuilder sql = new StringBuilder();
        Map<String,Object> params = new HashMap<>();
        sql.append(" insert into user_token(user_id,token,status) values(:user_id,:token,:status) ");

        params.put("user_id",userTokenEntity.getUserId());
        params.put("token",userTokenEntity.getToken());
        params.put("status",userTokenEntity.getStatus());

        return namedParameterJdbcTemplate.update(sql.toString(),params);
    }
}
