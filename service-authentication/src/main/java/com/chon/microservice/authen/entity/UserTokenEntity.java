package com.chon.microservice.authen.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserTokenEntity {
    private String userId;
    private String token;
    private String status;
}
