package com.chon.microservice.employee;

import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.EmployeeDto;
import com.chon.microservice.employee.entity.EmployeeEntity;
import com.chon.microservice.employee.mapper.EmployeeMapper;
import com.chon.microservice.employee.repository.EmployeeRepository;
import com.chon.microservice.employee.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
    @Mock
    private EmployeeMapper employeeMapper;

    @Mock
    private EmployeeRepository employeeRepository;


    private EmployeeService employeeService;

    @Before
    public void setUp() {
        employeeService = new EmployeeService(employeeMapper,employeeRepository);
    }

    @Test
    public void testInsertEmployee() {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setEmpId("003");
        employeeEntity.setEmpName("test");
        employeeEntity.setEmpLastname("test");

        doReturn(1).when(employeeRepository).insert(any(EmployeeEntity.class));
        doReturn(employeeEntity).when(employeeMapper).employeeDtoToEmployeeEntity(any(EmployeeDto.class));

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        CommonResponseDto result = employeeService.insertEmployee(employeeDto);

        assertEquals("200", result.getStatus());
    }

    @Test
    public void testUpdateEmployee() {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setEmpId("003");
        employeeEntity.setEmpName("test");
        employeeEntity.setEmpLastname("test");

        doReturn(1).when(employeeRepository).update(any(EmployeeEntity.class));
        doReturn(employeeEntity).when(employeeMapper).employeeDtoToEmployeeEntity(any(EmployeeDto.class));

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        CommonResponseDto result = employeeService.updateEmployee(employeeDto);

        assertEquals("200", result.getStatus());
    }

    @Test
    public void testFindAllEmployee() {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setEmpId("003");
        employeeEntity.setEmpName("test");
        employeeEntity.setEmpLastname("test");

        List<EmployeeEntity> employeeEntities = new ArrayList<>();
        employeeEntities.add(employeeEntity);
        employeeEntities.add(employeeEntity);

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        List<EmployeeDto> employeeDtos = new ArrayList<>();
        employeeDtos.add(employeeDto);
        employeeDtos.add(employeeDto);

        doReturn(employeeEntities).when(employeeRepository).findAll();
        doReturn(employeeDtos).when(employeeMapper).employeeEntitysToEmployeeDtos(employeeEntities);

        CommonResponseDto result = employeeService.findAll();

        assertEquals(2, ((List)result.getData()).size() );
    }
    @Test
    public void testFindByIdEmployee() {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setEmpId("003");
        employeeEntity.setEmpName("test");
        employeeEntity.setEmpLastname("test");


        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        doReturn(employeeEntity).when(employeeRepository).findById("003");
        doReturn(employeeDto).when(employeeMapper).employeeEntityToEmployeeDto(employeeEntity);

        CommonResponseDto result = employeeService.findById("003");

        assertEquals("003", ((EmployeeDto)result.getData()).getId() );
    }

    @Test
    public void testDeleteEmployee(){
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setEmpId("003");
        employeeEntity.setEmpName("test");
        employeeEntity.setEmpLastname("test");


        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setName("test");
        employeeDto.setLastName("test");
        employeeDto.setId("003");

        doReturn(1).when(employeeRepository).delete(employeeEntity);
        doReturn(employeeEntity).when(employeeMapper).employeeDtoToEmployeeEntity(employeeDto);

        CommonResponseDto result = employeeService.deleteEmployee(employeeDto);

        assertEquals("200", result.getStatus());
    }
}
