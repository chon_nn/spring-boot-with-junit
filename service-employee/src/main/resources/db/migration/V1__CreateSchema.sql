DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
  emp_id VARCHAR(32)  PRIMARY KEY,
  emp_name VARCHAR(250) NOT NULL,
  emp_lastname VARCHAR(250) NOT NULL
);

insert into employee(emp_id,emp_name,emp_lastname) values ('001','Test 1','Test 1');
insert into employee(emp_id,emp_name,emp_lastname) values ('002','Test 2','Test 2');