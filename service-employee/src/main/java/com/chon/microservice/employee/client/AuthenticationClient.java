package com.chon.microservice.employee.client;

import com.chon.microservice.employee.dto.CommonResponseDto;
import com.chon.microservice.employee.dto.TokenDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "service-authentication")
public interface AuthenticationClient {
    @PostMapping("/authentication/verify")
    CommonResponseDto verifyToken(@RequestBody TokenDto tokenDto);

}