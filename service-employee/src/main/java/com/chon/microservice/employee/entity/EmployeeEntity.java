package com.chon.microservice.employee.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeEntity {
    private String empId;
    private String empName;
    private String empLastname;
}
