package com.chon.microservice.employee.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResponseDto {
    private String status;
    private Object data;
}
