package com.chon.microservice.employee.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDto {
    private String id;
    private String name;
    private String lastName;
}
