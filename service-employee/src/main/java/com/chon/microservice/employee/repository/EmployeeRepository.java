package com.chon.microservice.employee.repository;

import com.chon.microservice.employee.entity.EmployeeEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class EmployeeRepository {
    final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Integer insert(EmployeeEntity employeeEntity){
        StringBuilder sql = new StringBuilder();
        sql.append(" insert into employee(emp_id,emp_name,emp_lastname) values(:emp_id,:emp_name,:emp_lastname) ");

        Map<String,Object> param = new HashMap<>();
        param.put("emp_id",employeeEntity.getEmpId());
        param.put("emp_name",employeeEntity.getEmpName());
        param.put("emp_lastname",employeeEntity.getEmpLastname());

        return namedParameterJdbcTemplate.update(sql.toString(),param);
    }
    public Integer update(EmployeeEntity employeeEntity){
        StringBuilder sql = new StringBuilder();
        sql.append("     update employee set ");
        sql.append("         emp_name = :emp_name, ");
        sql.append("         emp_lastname = :emp_lastname ");
        sql.append("     where emp_id = :emp_id   ");

        Map<String,Object> param = new HashMap<>();
        param.put("emp_id",employeeEntity.getEmpId());
        param.put("emp_name",employeeEntity.getEmpName());
        param.put("emp_lastname",employeeEntity.getEmpLastname());

        return namedParameterJdbcTemplate.update(sql.toString(),param);

    }

    public List<EmployeeEntity> findAll(){
        StringBuilder sql = new StringBuilder();
        sql.append(" select emp_id,emp_name,emp_lastname from employee ");

        Map<String,Object> params = new HashMap<>();

        return namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(EmployeeEntity.class));
    }

    public EmployeeEntity findById(String id){
        StringBuilder sql = new StringBuilder();
        sql.append(" select emp_id,emp_name,emp_lastname from employee where emp_id = :emp_id");

        Map<String,Object> params = new HashMap<>();
        params.put("emp_id",id);

        return namedParameterJdbcTemplate.queryForObject(sql.toString(),params,new BeanPropertyRowMapper<>(EmployeeEntity.class));
    }

    public Integer delete(EmployeeEntity employeeEntity){
        StringBuilder sql = new StringBuilder();
        sql.append(" delete from employee where emp_id = :emp_id ");

        Map<String,Object> param = new HashMap<>();
        param.put("emp_id",employeeEntity.getEmpId());

        return namedParameterJdbcTemplate.update(sql.toString(),param);
    }
}